package main

import "fmt"

func main() {
	n := 12
	// Read n from input
	DisplayMinimumNumberFunction(n)
}

// https://www.hackerrank.com/contests/w30/challenges/find-the-minimum-number
func DisplayMinimumNumberFunction(n int) {
	var addon string = ""
	var end string = ""
	var m int
	for m = 1; m < n; m++ {
		addon += "min(int, "
		end += ")"
	}
	fmt.Printf(addon + "int" + end)
}
