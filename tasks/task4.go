package main

import (
	"fmt"
	"strconv"
)

func main() {
	fmt.Println(MySquareRoot(9, 9))
}

func MySquareRoot(num, precision float64) (result float64) {
	var division float64 = num / 2
	var temp float64

	for {
		temp = division
		division = (temp + (num / temp)) / 2
		if (temp - division) == 0 {
			break
		}
	}
	h := fmt.Sprintf("%.12f", division)
	if s, err := strconv.ParseFloat(h, 64); err == nil {
		fmt.Println(s)
	}
	return
}
